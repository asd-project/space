
set(MANIFEST_PATHS "${CMAKE_CURRENT_SOURCE_DIR}" "${CMAKE_CURRENT_SOURCE_DIR}/../../export")

foreach(PATH ${MANIFEST_PATHS})
	if (EXISTS "${PATH}/conanfile.py" OR EXISTS "${PATH}/conanfile.txt")
		set(MANIFEST_PATH ${PATH})
	endif()
endforeach()

if ("${MANIFEST_PATH}" STREQUAL "")
	message(FATAL_ERROR "Couldn't find conanfile.py or conanfile.txt")
endif()

if (NOT EXISTS ${CMAKE_CURRENT_BINARY_DIR}/conanbuildinfo.txt)
	if ("${CMAKE_BUILD_TYPE}" STREQUAL "")
	    execute_process(COMMAND conan install -if ${CMAKE_CURRENT_BINARY_DIR} . -s build_type=Release --build missing RESULT_VARIABLE RESULT WORKING_DIRECTORY ${MANIFEST_PATH})
	else()
	    execute_process(COMMAND conan install -if ${CMAKE_CURRENT_BINARY_DIR} . -s build_type=${CMAKE_BUILD_TYPE} --build missing RESULT_VARIABLE RESULT WORKING_DIRECTORY ${MANIFEST_PATH})
    endif()
	
	if(NOT ${RESULT} EQUAL 0)
		message(FATAL_ERROR "Couldn't install conan manifest")
	endif()
endif()

function(locate_package package output_path)
    if (PACKAGE_PATH_${package})
        set(${output_path} "${PACKAGE_PATH_${package}}" PARENT_SCOPE)
        return()
    endif()

    execute_process(COMMAND conan editable list OUTPUT_VARIABLE editable_packages WORKING_DIRECTORY "${CMAKE_SOURCE_DIR}")

    if ("${editable_packages}" MATCHES "${package}/[.a-zA-Z0-9_-]+\@[.a-zA-Z0-9_-]+/[.a-zA-Z0-9_-]+[ \t\r\n]+Path:[ \t\r\n]+([ \\/.a-zA-Z0-9_-]+)[ \t\r\n]+")
        set(${output_path} "${CMAKE_MATCH_1}" PARENT_SCOPE)
        return()
    endif()

	execute_process(COMMAND conan info --package-filter ${package}/* --paths -n package_folder . OUTPUT_VARIABLE COMMAND_OUTPUT ERROR_VARIABLE COMMAND_OUTPUT RESULT_VARIABLE RESULT WORKING_DIRECTORY ${MANIFEST_PATH})
	
	if(NOT ${RESULT} EQUAL 0)
		message(FATAL_ERROR "Couldn't locate package: ${package} - ${COMMAND_OUTPUT}")
	endif()
	
	string(REGEX MATCH "package_folder:[ ]+(.*)$" _ "${COMMAND_OUTPUT}")
	string(STRIP "${CMAKE_MATCH_1}" package_folder)
	set(${output_path} "${package_folder}" PARENT_SCOPE)
endfunction()

locate_package(asd.build_tools BUILD_TOOLS_FOLDER)

include("${BUILD_TOOLS_FOLDER}/workspace.cmake")
include(module)
