import os

from conans import ConanFile, CMake

project_name = "space"


class SpaceConan(ConanFile):
    name = "asd.%s" % project_name
    version = "0.0.1"
    license = "MIT"
    author = "bright-composite"
    url = "https://gitlab.com/asd-project/%s" % project_name
    description = "Spacial objects and mathematical aliases"
    topics = ("asd", project_name)
    generators = "cmake"
    exports_sources = "include*", "src*", "CMakeLists.txt", "bootstrap.cmake"
    requires = (
        "asd.math/0.0.1@bright-composite/testing"
    )
    
    def source(self):
        pass
    
    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()
    
    def package(self):
        self.copy("*.h", dst="include", src="include")
